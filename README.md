Table of Contents
=================

  * [Intro](#Intro)
  * [Using](#Using)

# Intro

Puppet CI tooling including:
  - Puppet Enterprise
  - CD4PE
  - Jenkins
  - Agent node

# Using

```
R10K_REMOTE=git@github.com:<PROJECT>/control-repo.git vagrant up 
```