# Puppetserver role

class roles::puppetserver {
  contain profiles::base::linux::ntp
  contain profiles::puppetserver::base
  contain profiles::puppetserver::code_manager

  Class['profiles::puppetserver::base']->
  Class['profiles::base::linux::ntp']->
  Class['profiles::puppetserver::code_manager']

}

