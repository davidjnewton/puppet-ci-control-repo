# Manage operation of puppetserver
class profiles::puppetserver::base {

  $puppetserver_service_name = 'puppetserver'
  $puppet_conf_file          = '/etc/puppetlabs/puppet/puppet.conf' 
  $rmcert_script             = '/etc/puppetlabs/puppetserver/rmcert.sh'
  if $psk == undef { $psk = '123' }
  if $repohost_fqdn  == undef { $repohost_fqdn    = "repohost.${::domain}" }
  $package_name              = 'puppetserver'
  $package_version           = '6.3.0-1.el7'
  $service_name              = 'puppetserver'
  $autosign_sh_file          = '/etc/puppetlabs/puppet/autosign.sh'
  $autosign_psk_file         = '/etc/puppetlabs/puppet/global-psk'
  $hiera_config              = '/etc/puppetlabs/code/environments/production/hiera.yaml'
  $accept_tcp_ports          = ['8140','443','61613','8142','4433']
  $puppetserver_conf_file    = '/etc/puppetlabs/puppetserver/conf.d/puppetserver.conf' 
  $autosign_bash             = @(AUTOSIGNBASH)
    #!/bin/bash   
    csr=$(< /dev/stdin)
    certname=$1
    textformat=$(echo "$csr" | openssl req -noout -text)
    global_psk=$(cat /etc/puppetlabs/puppet/global-psk)
    if [ "$(echo $textformat | grep -Po $global_psk)" = "$global_psk" ]; then
      echo -e "CSR Stdin contains: $csr \n\nInfo: Autosigning $certname with global-psk $global_psk..." >> /tmp/autosign.log
      exit 0
    else
      echo -e "CSR Stdin contains: $csr \n\nWarn: Not Autosigning $certname with global-psk $global_psk - no match." >> /tmp/autosign.log
      exit 1
    fi
    | AUTOSIGNBASH
  
  file {'/etc/puppetlabs/puppet/hiera.yaml':
    ensure => absent,
  }
  
  $accept_tcp_ports.each |$index, $port| {
    firewall { "10${index} accept tcp dport ${port}":
      action => 'accept',
      proto  => 'tcp',
      dport  => $port,
    }
  }
 
  package { $package_name:
    ensure  => $package_version,
  }

  file { $autosign_psk_file:
    require => Package[$package_name],
    ensure  => file,
    content => $psk,
  }

  file { $autosign_sh_file:
    require => Package[$package_name],
    ensure  => file,
    content => $autosign_bash,
    mode    => '0755',
  }

  ini_setting { "autosign in ${puppet_conf_file}":
    require           => Package[$package_name],
    ensure            => present,
    path              => $puppet_conf_file,
    section           => 'master',
    setting           => 'autosign',
    key_val_separator => '=',
    value             => $autosign_sh_file,
    notify            => Service[$puppetserver_service_name],
  }
  
  file { '/opt/puppetlabs/server/data/puppetserver/grep-reports.sh':
    require => Package[$package_name],
    ensure  => file,
    content => epp('profiles/puppetserver/grep-reports.sh.epp'),
    mode    => '0755',
  }

  service { $puppetserver_service_name:
    enable    => true,
    ensure    => running,
  }
  
  # clean reports older than 300 minutes
  cron { 'clean_reports':
    command => [
      '/usr/bin/find /opt/puppetlabs/server/data/puppetserver/reports -type f -mmin +300',
      '-exec /usr/bin/logger "[cron] Cleaning report: {}" \; -exec /usr/bin/rm -fv {} \;',
    ].join(' '),
    user    => root,
    minute  => '10',
  }

  # Setup xinetd endpoint to allow automated cert removal
  file { $rmcert_script:
    ensure  => file,
    content => epp('profiles/puppetserver/rmcert.sh.epp'),
    mode    => '0500',
    owner   => 'root',
  }
  xinetd::service { 'http':
    require     => File[$rmcert_script],
    port        => '80',
    socket_type => 'stream',
    protocol    => 'tcp',
    wait        => 'no',
    user        => 'root',
    server      => $rmcert_script,
  }

}

