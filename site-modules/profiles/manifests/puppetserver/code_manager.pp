# R10k setup
#
class profiles::puppetserver::code_manager {
  if $git_url             == undef { fail('ERROR: \$git_url empty/unset')  }
  if $repohost_fqdn       == undef { $repohost_fqdn         = "repohost.${::domain}" }
  if $gem_source_url      == undef { notice("not managing gem sources - \$gem_source_url not defined.") }
  if $r10k_cron_minute    == undef { $r10k_cron_minute      = '10' }
  $r10k_sshkey_file          = '/etc/puppetlabs/r10k/r10k_id_rsa'
  $r10k_deploy_sh_file       = '/etc/puppetlabs/r10k/r10k_deploy.sh'
  $r10k_deploy_command       = [
    '/usr/bin/logger "[cron] Starting r10k deploy..."',
    "&& ${r10k_deploy_sh_file} ${r10k_sshkey_file}",
    '; /usr/bin/logger "[cron] r10k deploy finished with exit code=$?."' ,
  ].join(' ')

  unless $gem_source_url == undef {
    # Fix for puppet gem source defaulting to rubygems.org
    exec { "gem sources --remove https://rubygems.org":
      path    => '/opt/puppetlabs/puppet/bin:/usr/bin',   
      onlyif  => "gem sources --list | grep -o https://rubygems.org",
      before  => Class['ruby'],
    } 
    exec { "gem sources --add ${gem_source_url}":
      require => Exec["gem sources --remove https://rubygems.org"],
      path    => '/opt/puppetlabs/puppet/bin:/usr/bin',
      unless  => "gem sources --list | grep -o ${gem_source_url}",
      before  => [ Class['ruby'], Class['r10k'] ]
    }
    class { '::ruby::gemrc': 
      sources => [$gem_source_url],
      #before  => Class['ruby::dev']
    }
  }

  class { '::ruby': }
  
  #class { '::ruby::dev': }

  class {'r10k': 
    require         => Class['::ruby'],
    git_settings    => { 
      'provider'    => 'shellgit',
    #  'private_key' => $r10k_sshkey_file,
    },
    remote                 => $git_url,
    manage_ruby_dependency => 'ignore',
  }

  $r10k_deploy_sh_content = @(EOF)
    #!/bin/sh
    PRIVATE_KEY=$1
    eval "$(ssh-agent -s)"
    trap "ssh-agent -k" exit
    ssh-add $PRIVATE_KEY
    r10k deploy environment --puppetfile -v
    | EOF

  file { $r10k_deploy_sh_file:
    ensure  => file,
    content => inline_epp($r10k_deploy_sh_content),
    mode    => '0755',
    owner   => 'root',
    group   => 'root',
  }

  cron { 'r10k_deploy':
    user    => 'root',
    command => $r10k_deploy_command,
    minute  => $r10k_cron_minute,
    notify  => Exec['r10k first-time deploy'],
  }
  
  exec { 'r10k first-time deploy':
    refreshonly => true,
    command     => $r10k_deploy_command,
  }

}
