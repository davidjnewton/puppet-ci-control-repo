# ntp

class profiles::base::linux::ntp {
  $global_ntp_servers = lookup('profiles::base::global_ntp_servers', Array, first, ['143.210.16.201'])
  $package_manage     = lookup('profiles::base::linux::ntp::package_manage', Boolean, first, false)
  
  class { '::ntp':
    servers        => $global_ntp_servers,
    package_manage => $package_manage,
  }
}
