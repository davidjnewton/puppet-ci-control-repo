#!/bin/bash

srcdir="/usr/share/jenkins"
jenkinsdir="/var/lib/jenkins"
user="admin"
passwd=`cat /var/jenkins_home/secrets/initialAdminPassword`
newpasswd="jenkins"
port="8080"
url="localhost:$port"
until [ -f $srcdir/jenkins-cli.jar ] ; do { wget -P $srcdir http://$url/jnlpJars/jenkins-cli.jar --retry-connrefused --waitretry=5 --read-timeout=20 --timeout=15 -t 10 ; sleep 3 ;}; done
java -jar $srcdir/jenkins-cli.jar -s http://$url who-am-i --username $user --password $passwd
echo "jenkins.model.Jenkins.instance.securityRealm.createAccount("\'"$user"\'","\'"$newpasswd"\'")" | java -jar $srcdir/jenkins-cli.jar -auth admin:$passwd -s http://localhost:$port groovy =
CRUMB=$(curl -s "http://$user:$newpasswd@$url/crumbIssuer/api/xml?xpath=concat(//crumbRequestField,\":\",//crumb)")
#install plugins
#echo $CRUMBcurl -H "$CRUMB" -X POST -d '<jenkins><install plugin="git@2.0" /></jenkins>' --header 'Content-Type: text/xml' 'http://'$user':'$newpasswd'@localhost:8080/pluginManager/installNecessaryPlugins'
