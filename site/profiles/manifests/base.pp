#The base profile includes configuration for all nodes

class profiles::base {

  $sysctl_hash     = hiera('profiles::base::sysctl_hash')
  $ntp_servers     = hiera('profiles::base::ntp_servers')
  $dns_nameservers = hiera('profiles::base::dns_nameservers')

  $sysctl_defaults = {
    'ensure'   => 'present',
    'permanent' => 'yes',
  }

  create_resources(sysctl, $sysctl_hash, $sysctl_defaults)

  class { 'ntp':
    servers     => $ntp_servers,
  }

  class { 'dnsclient':
    nameservers => $dns_nameservers,
  }

}