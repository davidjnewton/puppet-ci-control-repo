# jenkins master
class profiles::jenkins_master (
  String           $version        = 'lts',
  String           $docker_version = '18.09.3-3.el7',
  String[1]        $jobs_root      = '/var/lib/docker/volumes/jenkins_home/_data/jobs',
  Array[String[1]] $jobs           = [
    'puppet-control-repo',
    'puppet-jenkins-docker-image',
  ],
) {

  class { 'docker':
    storage_driver => 'overlay2',
    version        => $docker_version,
  }

  docker::image { 'jenkins/jenkins':
    image_tag => $version,
  }

  docker::run { 'jenkins':
    image      => "jenkins/jenkins:${version}",
    username   => 'root',
    privileged => true,
    ports      => ['8080:8080','50000:50000'],
    volumes    => [
      'jenkins_home:/var/jenkins_home',
      '/bin/docker:/usr/bin/docker',
      '/var/run/docker.sock:/var/run/docker.sock',
    ],
  }

  file { $jobs_root:
    ensure  => directory,
    require => Docker::Run['jenkins'],
  }

  $jobs.each|$job|{
    file { "${jobs_root}/${job}":
      ensure => directory,
      mode   => '0755',
      group  => root,
      owner  => root,
    }
    file { "${jobs_root}/${job}/config.xml":
      source => "puppet:///modules/profiles/jenkins/jobs/${job}.xml",
      mode   => '0644',
      group  => root,
      owner  => root,
    }
  }

  file { '/var/lib/docker/volumes/jenkins_home/_data/bootstrap_admin_account.sh':
    require => Docker::Run['jenkins'],
    source  => 'puppet:///modules/profiles/jenkins/bootstrap_admin_account.sh',
    mode    => '0755'
  }
}
