#The profile includes configuration for backup 

class profiles::backup {

  class { 'backup': }

  backup::job { '/etc/puppetlabs':
    types        => 'archive',
    add          => '/etc/puppetlabs',
    storage_type => 'local',
    path         => '/backups',
    weekday      => 0,
    hour         => 3,
    minute       => 0,
  }

  backup::job { '/opt/puppetlabs/server/data/console-services/certs/':
    types        => 'archive',
    add          => '/opt/puppetlabs/server/data/console-services/certs/',
    storage_type => 'local',
    path         => '/backups',
    weekday      => 0,
    hour         => 3,
    minute       => 10,
  }

  backup::job { '/opt/puppetlabs/server/data/postgresql/9.4/data/certs':
    types        => 'archive',
    add          => '/opt/puppetlabs/server/data/postgresql/9.4/data/certs',
    storage_type => 'local',
    path         => '/backups',
    weekday      => 0,
    hour         => 3,
    minute       => 20,
  }

  backup::job { '/etc/puppetlabs/puppetserver/conf.d/':
    types        => 'archive',
    add          => '/etc/puppetlabs/puppetserver/conf.d/',
    storage_type => 'local',
    path         => '/backups',
    weekday      => 0,
    hour         => 3,
    minute       => 30,
  }

}
