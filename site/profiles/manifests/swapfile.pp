#The profile includes configuration for swapfile 

class profiles::swapfile {

  swap_file::files { 'tmp file swap':
    ensure       => present,
    swapfile     => '/tmp/swapfile',
    add_mount    => false,
    swapfilesize => '2 GB',
  }

}
