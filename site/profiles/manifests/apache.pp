#The profile includes configuration for apache and mods

class profiles::apache {

  $default_mods = hiera('profiles::apache::default_mods')
  class { 'apache':
    mpm_module => 'prefork',
  }
  $default_mods.each |String $mod|
  {
    include "apache::mod::${mod}"
  }

}
