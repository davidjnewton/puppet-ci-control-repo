#The profile includes configuration for tomcat 

class profiles::tomcat {

  tomcat::install { '/opt/tomcat':
    source_url => 'http://www-us.apache.org/dist/tomcat/tomcat-8/v8.5.12/bin/apache-tomcat-8.5.12.tar.gz',
  }

}
