#!/opt/puppetlabs/puppet/bin/ruby

require 'open3'
require 'json'

$params       = JSON.parse(STDIN.read)
$target       = $params['target']
$plan         = $params['plan']
$plan_params  = $params['plan_params']
$bolt         = '/opt/puppetlabs/server/apps/bolt-server/bin/bolt'
$puppet_bin   = '/opt/puppetlabs/puppet/bin'
$environment  = 'production'
$modulepath   = Open3.capture3("#{$puppet_bin}/puppet", 'config', 'print', 'modulepath').first.strip
$boltdir      = Open3.capture3("#{$puppet_bin}/puppet", 'print', 'codedir').first.strip + "/environments/#{$environment}"
$gem_path     = "/opt/puppetlabs/server/apps/bolt-server/lib/ruby:/opt/puppetlabs/puppet/lib/ruby/vendor_gems" #+ Open3.capture3("#{$puppet_bin}/gem", 'env', 'gempath').first.strip
$result       = {}

if ENV['HOME'].nil?
  require 'etc'
  ENV['HOME'] = Etc.getpwuid.dir
end

$bolt_env = {
  'GEM_PATH' => $gem_path,
  'HOME'     => "#{ENV['HOME']}",
}

$bolt_cmd = Array.new
$bolt_cmd << $bolt << 'plan' << 'run' << $plan
$bolt_cmd << "--modulepath=#{$modulepath}"
$bolt_cmd << "--boltdir=#{$boltdir}"
$bolt_cmd << '--params' << $plan_params.to_json

$keys = ['stdout', 'stderr', 'status']
$result = Hash[$keys.zip(Open3.capture3($bolt_env, "#{$puppet_bin}/gem env"))]
#$result = Hash[$keys.zip(Open3.capture3($bolt_env, *$bolt_cmd))]

puts $result.to_json
