#The webserver role

class roles::webserver {
  include profiles::apache
  include profiles::tomcat
  Class['profiles::apache']
  ->Class['profiles::tomcat']
}
