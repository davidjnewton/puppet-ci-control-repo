#-*- mode: ruby -*-
# vi: set ft=ruby :
#
#$my_ip="192.168.43.199"#"#{(`/usr/bin/facter ipaddress`).gsub(/\n/,"")}"
#R10K_REMOTE=git@github.com:<PROJECT>/control-repo.git

VAGRANT_GUEST_FOLDER        = ENV['VAGRANT_GUEST_FOLDER']        || '/tmp/vagrant'
PE_INSTALLER_URL            = ENV['PE_INSTALLER_URL']            || 'https://s3.amazonaws.com/pe-builds/released/2019.0.2/puppet-enterprise-2019.0.2-el-7-x86_64.tar.gz' 
PE_INSTALLER_SRC            = ENV['PE_INSTALLER_SRC']            || "#{VAGRANT_GUEST_FOLDER}/packages/pe/latest/puppet-enterprise-installer.tar.gz"
PUPPET_MASTER_SERVER        = ENV['PUPPET_MASTER_SERVER']        || 'puppet.local'
PUPPET_CODEDIR              = ENV['PUPPET_CODEDIR']              || '/etc/puppetlabs/code'
PUPPET_USER                 = ENV['PUPPET_USER']                 || 'pe-puppet'
PUPPET_GROUP                = ENV['PUPPET_GROUP']                || 'pe-puppet'
NODENAME                    = ENV['NODE']                        || 'agent'
R10K_REMOTE                 = ENV['R10K_REMOTE']                 || ''
R10K_PRIVATE_KEY_LOCAL      = ENV['R10K_PRIVATE_KEY_LOCAL']      || "#{VAGRANT_GUEST_FOLDER}/site/profiles/files/r10k/r10k_id_rsa_example"
R10K_PRIVATE_KEY            = ENV['R10K_PRIVATE_KEY']            || '/etc/puppetlabs/r10k/r10k_key' # copied from synced: R10K_PRIVATE_KEY_LOCAL
CODE_MANAGER_AUTO_CONFIGURE = ENV['CODE_MANAGER_AUTO_CONFIGURE'] || 'true'
CD4PE_IP                    = ENV['CD4PE_IP']                    || '192.168.100.111'
DISTELLI_ACCESS_TOKEN       = ENV['DISTELLI_ACCESS_TOKEN']       || 'R6563Y1VO9NV0PYVO8SASTVJI'
DISTELLI_SECRET_KEY         = ENV['DISTELLI_SECRET_KEY']         || 'esbo2kc9cr1e1yu5lretd40ca4ilm4c777j8y'
DOCKER_VERSION              = ENV['DOCKER_VERSION']              || '18.06.1.ce-3.el7'

nodes = [
  {
    :hostname        => 'puppet',
    :ip              => '192.168.100.110',
    :domain          => 'local',
    :box             => 'puppetlabs/centos-7.0-64-nocm',
    :ssh_username    => 'root',
    :ssh_password    => 'puppet',
    :ram             => 4000,
    :cpus            => 2,
    :cpuexecutioncap => 75,
    :shell_script    => "echo \"Installing and configuring PE...\"                                                      \
      && echo '{ \"console_admin_password\": \"puppet\",                                                                \
        \"puppet_enterprise::puppet_master_host\": \"%{::trusted.certname}\",                                           \
        \"puppet_enterprise::profile::master::r10k_remote\": \"#{R10K_REMOTE}\",                                        \
        \"puppet_enterprise::profile::master::r10k_private_key\": \"#{R10K_PRIVATE_KEY}\",                              \
        \"puppet_enterprise::profile::master::code_manager_auto_configure\": #{CODE_MANAGER_AUTO_CONFIGURE}, }'         \
        > pe.conf                                                                                                       \
      && if ! test -f '#{PE_INSTALLER_SRC}' ; then echo 'downloding #{PE_INSTALLER_URL}...'                             \
      ; SRC='#{PE_INSTALLER_SRC}' && mkdir -p ${SRC%/*} && wget -qk '#{PE_INSTALLER_URL}' -O '#{PE_INSTALLER_SRC}' ; fi \
      && mkdir puppet-enterprise-installer                                                                              \
      && tar -xf '#{PE_INSTALLER_SRC}' -C puppet-enterprise-installer --strip-components 1                              \
      && sudo puppet-enterprise-installer/puppet-enterprise-installer -c pe.conf                                        \
      && if test -f '#{R10K_PRIVATE_KEY_LOCAL}' ; then cp '#{R10K_PRIVATE_KEY_LOCAL}' '#{R10K_PRIVATE_KEY}'             \
      && chown pe-puppet:pe-puppet #{R10K_PRIVATE_KEY} && chmod 640 #{R10K_PRIVATE_KEY} ; fi                            \
      && PUPPETBIN=/opt/puppetlabs/puppet/bin/puppet                                                                    \
      && $PUPPETBIN resource service firewalld ensure=stopped enable=false                                              \
      && $PUPPETBIN config set autosign true --section=master && service pe-puppetserver restart                        \
      && $PUPPETBIN agent -tv ; $PUPPETBIN agent -tv ; $PUPPETBIN agent -tv                                             \
      ; echo 'puppet' | sudo -i $PUPPETBIN access login -l 180d --username admin                                                \
      && if [ ! -z '#{R10K_REMOTE}' ] ; then sudo -i $PUPPETBIN code deploy --all --wait ; fi                                   \
    ",
    :shell_args      => [],
  },
  {
    :hostname        => 'ci-master',
    :ip              => '192.168.100.113',
    :domain          => 'local',
    :box             => 'centos/7',
    :ram             => 4000,
    :cpus            => 2,
    :cpuexecutioncap => 75,
    :noscp           => true,
    :shell_script    => "echo \"Installing Puppet...\"                                                                        \
      && echo '192.168.100.110    puppet.local puppet' >> /etc/hosts                                                          \
      && curl -k https://puppet.local:8140/packages/current/install.bash | sudo bash -s extension_requests:pp_role=ci_master  \
      && PUPPETBIN=/opt/puppetlabs/puppet/bin/puppet                                                                          \
      && $PUPPETBIN resource service firewalld ensure=stopped enable=false                                                    \
      &&  $PUPPETBIN agent -t                                                                                                 \
      ; while [ -f /opt/puppetlabs/puppet/cache/state/agent_catalog_run.lock ] ; do sleep 3 ; done                            \
      &&  $PUPPETBIN agent -t                                                                                                 \
      ; while [ ! -f /var/lib/docker/volumes/jenkins_home/_data/secrets/initialAdminPassword ] ; do sleep 3 ; done            \
      && docker exec jenkins /var/jenkins_home/bootstrap_admin_account.sh                                                     \
      && service docker-jenkins restart                                                                                       \
    ",
    :shell_args      => [],
  },
]

Vagrant.configure("2") do |config|
  nodes.each do |node|
    config.vm.define node[:hostname] do |nodeconfig|
      nodeconfig.vm.box       = node[:box]
      nodeconfig.vm.hostname  = node[:domain] ? "#{node[:hostname]}.#{node[:domain]}" : "#{node[:hostname]}" ;
      memory                  = node[:ram] ? node[:ram] : 1000 ; 
      cpus                    = node[:cpus] ? node[:cpus] : 2 ;
      cpuexecutioncap         = node[:cpuexecutioncap] ? node[:cpuexecutioncap] : 50 ;
      syncedfolder_host       = node[:syncedfolder_host] ? node[:syncedfolder_host] : "." ;
      syncedfolder_guest      = node[:syncedfolder_guest] ? node[:syncedfolder_guest] : "/vagrant" ;
      nodeconfig.ssh.username = node[:ssh_username] if node[:ssh_username]
      nodeconfig.ssh.password = node[:ssh_password] if node[:ssh_password]
      nodeconfig.vm.network :private_network, ip: node[:ip]
      nodeconfig.vm.provider :virtualbox do |vb|
        vb.customize [
          "modifyvm", :id,
          "--memory", memory.to_s,
          "--cpus", cpus.to_s,
          "--cpuexecutioncap", cpuexecutioncap.to_s,
        ] 
      end 
      if node[:syncedfolder_host] and node[:syncedfolder_guest] then 
        config.vm.provision "file", source: node[:syncedfolder_host], destination: node[:syncedfolder_guest]
        #nodeconfig.vm.synced_folder node[:syncedfolder_host], node[:syncedfolder_guest],  id: "vagrant-root"
      end
      unless node[:noscp] then
        config.vm.provision "file", source: ".", destination: "#{VAGRANT_GUEST_FOLDER}"
        config.vm.provision "file", source: "../packages", destination: "#{VAGRANT_GUEST_FOLDER}/packages"
        #nodeconfig.vm.synced_folder ".", "#{VAGRANT_GUEST_FOLDER}"
      end
      nodeconfig.vm.provision "shell" do | s |
        s.inline     = node[:shell_script]
        s.args       = node[:shell_args]
        s.keep_color = true
      end
    end
  end
end





